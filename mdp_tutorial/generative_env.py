from typing import List, Tuple

from abc import ABC, abstractmethod

import torch as th

class GenerativeEnv(ABC):
    
    @abstractmethod
    def sample_action(self, batch_size: int) -> th.Tensor:
        '''
        sample actions uniformly from action space
        @param batch_size: number of actions to sample
        @return tensor of states sampled uniformly from action space
        '''
        return NotImplemented
    

    @abstractmethod
    def sample_state(self, batch_size: int) -> th.Tensor:
        '''
        sample states from reachable state space
        @param batch_size: number of states to sample
        @return tensor of states sampled uniformly from reachable state space
        '''
        return NotImplemented

    @abstractmethod
    def generative_step(self, state: th.Tensor, action: th.Tensor) -> Tuple[th.Tensor, th.Tensor, th.Tensor, List[dict]]:
        '''
        Perform one step in the environment, starting at state and performing action
        @param state: Tensor of states from which the steps are taken
        @param action: Tensor of actions to perform
        @return: Tuple of outputes defining what happend in the step.
          1:  float tensor of next states
          2:  float tensor of rewards
          3:  boolean tensor of isdones (did the MDP terminate)
          4:  list of dictionaries contianing extra information (can generally be ignored)
        '''
        return NotImplemented

