{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MDP Tutorial -- Generative Edition\n",
    "\n",
    "When given an MDP, the task is generally to find a policy which will collect\n",
    "a lot of reward. In order to do this, two things need to be accomplished\n",
    "* characteristics of the MDP must be discovered (i.e. transition / reward dyanics), or at least areas of high values\n",
    "* a good policy must be found for the MDP\n",
    "\n",
    "If one assumes one already knows the MDP, that it doesn't need to be discovered,\n",
    "then things become simpler, as there's no need for exploration.\n",
    "\n",
    "Therefore, today we will learn about the Value function, Q-function and policies by\n",
    "using a so called **generative model** of an MDP. With a normal MDP, in\n",
    "order to discover the MDP's behavior at a state $s\\in\\mathcal S$ one must\n",
    "first navigate to that state. A generative MDP on the other hand allows\n",
    "us to sample the reward function $R(s,a)$ then the state transition distribution\n",
    "$\\mathbb P(\\cdot\\mid s,a)$ whereever, whenever and as much as we like. One can\n",
    "compare this somewhat to RAM vs. a (disk) hard drive on a computer. While\n",
    "RAM allows easy access to any block of memory, a hard drive must first go\n",
    "to all the trouble of getting the reading head over the relevant position\n",
    "of the disk.\n",
    "\n",
    "This isn't generally a realistic setting, but since it eleminates any need\n",
    "for exploration it makes other tasks (learning value functions, evaluating\n",
    "policies....) easier and is a good first step into the world of MDPs.\n",
    "Strictly speaking, this is then more what's called dynamic programming and\n",
    "not reinforcement learning, but a good reinforcement learner must also be an\n",
    "expert at dynamic programming.\n",
    "\n",
    "Now learning a good policy generally happens by initializing a policy $\\pi$ then\n",
    "alternating in two steps:\n",
    "* A value function $V^\\pi$ or a Q-function $Q^\\pi$ is learned for policy $\\pi$\n",
    "* A new policy is learned that performs well on $V^\\pi$ or $Q^\\pi$\n",
    "\n",
    "In this tutorial, we'll focus on the steps needed to do the steps above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Callable\n",
    "\n",
    "import numpy as np\n",
    "import gym\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.cm as cm\n",
    "import torch as th\n",
    "\n",
    "from gym.envs.registration import register\n",
    "from IPython.display import HTML\n",
    "\n",
    "from cartpole_generative import heuristic_policy_np, heuristic_policy_th\n",
    "from generative_env import GenerativeEnv\n",
    "from utils import VideoManager\n",
    "\n",
    "plt.rcParams['figure.figsize'] = [12, 12]\n",
    "\n",
    "register(\n",
    "    id=\"CartPoleGenerative-v0\",\n",
    "    entry_point=\"cartpole_generative:CartPoleGenerativeEnv\",\n",
    "    max_episode_steps=200,\n",
    "    reward_threshold=195.0,\n",
    ")\n",
    "cartpoleenv = gym.make('CartPoleGenerative-v0')\n",
    "vm = VideoManager() # necessary evil, browser likes to cache videos."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get to know the CartPole environment, which is nicely explained in the\n",
    "source code on [GitHub](https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py).\n",
    "Here, we use the `display_episode` function to display a few episodes\n",
    "under a random policy. Ignore any OpenCV warnings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html, rew_sum = vm.display_episode(cartpoleenv, lambda x : cartpoleenv.action_space.sample())\n",
    "print('rew_sum = ', rew_sum)\n",
    "HTML(html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Gere, we use the `display_episode` function to display a few episodes.\n",
    "using a heuristic policy (look at code if you want). Ignore any OpenCV warnings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html, rew_sum = vm.display_episode(cartpoleenv, heuristic_policy_np)\n",
    "print('rew_sum = ', rew_sum)\n",
    "HTML(html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculate the true value function\n",
    "\n",
    "Let's start by calculating the true value function of a few policies.\n",
    "Remember, the value function is the expected sum of discounted rewards\n",
    "$$V^{\\pi}(s):=\\mathbb E_{a_{t}\\sim\\pi(s_{t}),s_{t+1}\\sim\\mathbb P(\\cdot\\mid s_{t},a_t)}\\left[\\sum_{t=0}^\\infty \\gamma^t R(a_t, s_t)\\mid s_0=s\\right]$$.\n",
    "see documentation in `generative_env.py` for proper stepping through a generative MDP.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_value(env: GenerativeEnv,\n",
    "                    states: th.Tensor,\n",
    "                    policy: Callable[[th.Tensor], th.Tensor],\n",
    "                    gamma: float,\n",
    "                    num_evals: int = 1) -> th.Tensor:\n",
    "    '''\n",
    "    Given an environment and a tensor of states and a policy, estimate the discounted value generated by\n",
    "    that policy on the environment at the states.\n",
    "    @param env: environment \n",
    "    '''\n",
    "    # one reward for each state\n",
    "    reward_sum = th.zeros(states.size()[0])\n",
    "    \n",
    "    for eval in range(num_evals):\n",
    "        # start at starting states\n",
    "        obs = th.clone(states)\n",
    "        # reset tracker (everything is alive)\n",
    "        # keep track of the episode ending\n",
    "        isalive_tracker = th.ones(states.size()[0])\n",
    "        for t in range(200):\n",
    "            # use the policy to come up with an action\n",
    "            action = policy(obs)\n",
    "            # use env.generative_step (see documentation in generative_env.py) evaluate the mdp\n",
    "            obs, rew, isdone, _ = env.generative_step(obs, action)\n",
    "            \n",
    "            # add the discounted reward if episode isn't done\n",
    "            reward_sum += (gamma ** t) * isalive_tracker * rew\n",
    "            \n",
    "            # keep track of the episode ending\n",
    "            \n",
    "            isalive_tracker *= 1. - isdone.to(dtype=th.float32)\n",
    "        \n",
    "    # return the mean of the rewards\n",
    "    return reward_sum / num_evals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_value(env_local, calculate_value_local, policy_local, gamma, num_evals=1):\n",
    "    N = 200\n",
    "    x_dot     = np.linspace(-1., 1., N)\n",
    "    theta_dot = np.linspace(-1., 1., N)\n",
    "\n",
    "    x_dot_g, theta_dot_g = np.meshgrid(x_dot, theta_dot)\n",
    "    x_dot_g = x_dot_g.flatten()\n",
    "    theta_dot_g = theta_dot_g.flatten()\n",
    "    state_np = np.stack([np.zeros_like(x_dot_g), x_dot_g, np.zeros_like(x_dot_g), theta_dot_g], axis=1)\n",
    "    state_th = th.tensor(state_np, dtype=th.float32)\n",
    "    value = calculate_value_local(env_local, state_th, policy_local, gamma, num_evals)\n",
    "    mx = th.max(value).item()\n",
    "    print('max val = ', mx)\n",
    "    print('mean val = ', th.mean(value).item())\n",
    "    print('min val = ', th.min(value).item())\n",
    "    extent = (-1., 1., -1., 1.)\n",
    "    plt.imshow(np.reshape(value.numpy(), (N, N)), cmap=cm.jet, vmin=0., vmax=mx, origin='lower', extent=extent)\n",
    "    plt.xlabel(\"starting velocity\")\n",
    "    plt.ylabel(\"starting angular velocity\")\n",
    "    plt.show()\n",
    "    plt.clf()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## plot the true value function with the heuristic policy\n",
    "\n",
    "We can only plot slices, so we only vary the speed cart and the rotational speed.\n",
    "\n",
    "If you've done everyting correct, you should see a very jumpy plot with a maximum value of\n",
    "around 85. This jumpy nature of the value function is annoying, but to be expected, as\n",
    "demonstrated in [this paper](https://arxiv.org/abs/1910.05927)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plot_value(cartpoleenv, calculate_value, heuristic_policy_th, 1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now plot the true discounted value function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_value(cartpoleenv, calculate_value, heuristic_policy_th, .975)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What's the big difference between the discounted and undiscounted value functions\n",
    "\n",
    "In the literature, one always discounts value functions. Looking at the plots above,\n",
    "why do you think that is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learning the Q-function with the Bellman equation\n",
    "\n",
    "The Bellman equation states that the value of taking an action $a$ at state $s$ \n",
    "is equal to the reward collected by action $a$ at $s$ plus the value collected\n",
    "from following rewards. In formulas it is\n",
    "$$Q^{\\pi}(s,a)= R(s,a) + \\gamma\\mathbb E_{a'\\sim\\pi(s'),s'\\sim\\mathbb P(\\cdot\\mid s,a)}[Q^\\pi(s',a')].$$\n",
    "We can avoid having to learn both a policy and a $Q$-function by instead learning\n",
    "$Q^*$, the $Q$-function of the optimal policy. For $Q^*$ it holds (assuming $\\mathcal A$ is compact)\n",
    "$$Q^{*}(s,a)=R(s,a) + \\gamma\\mathbb E_{s'\\sim\\mathbb P(\\cdot\\mid s,a)}[\\max_{a'\\in\\mathcal A}Q^*(s',a')].$$\n",
    "In theory, we could estimate $Q^{*}$ with a neural network $\\hat Q^{*}_\\theta$\n",
    "and learn the parameters $\\theta$ by sampling a state $s$ from the state space,\n",
    "an action $a$ from the action space, observing reward $r\\sim R(s,a)$ and \n",
    "next state $s'\\sim\\mathbb P(\\cdot\\mid s,a)$ and performing updates via\n",
    "$$\\theta \\leftarrow \\theta - \\alpha\\nabla_\\theta(\\hat Q^{*}_\\theta(s,a) - (r + \\gamma\\max_{a'}\\hat Q^{*}_\\theta(s',a')))^2.$$\n",
    "Unfortunately. this isn't numerically stable. A simple heuristic to fix this is\n",
    "to define two neural networks $\\hat Q^{*}_\\theta$ and $\\hat Q^{*}_{\\theta'}$, update $\\theta$ via\n",
    "$$\\theta \\leftarrow \\theta - \\alpha\\nabla_\\theta(Q^{*}_\\theta(s,a) - (r + \\gamma \\max_{a'}\\hat Q^{*}_{\\theta'}(s',a')))^2.$$\n",
    "and periodically (i.e. every 1000 mini-batches) set $\\theta'\\leftarrow\\theta$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from stable_baselines3.common.torch_layers import create_mlp\n",
    "from stable_baselines3.common.utils import polyak_update\n",
    "\n",
    "class QFunction(object):\n",
    "    def __init__(self, gen_env, gamma):\n",
    "        self.batch_size = 256\n",
    "        self.gen_env = gen_env\n",
    "        self.gamma = gamma\n",
    "        self.max_val = 1. / (1. - self.gamma)\n",
    "        self.target_q_net = th.nn.Sequential(*create_mlp(6, 1, [128,128]))\n",
    "        self.estimated_q_net = th.nn.Sequential(*create_mlp(6, 1, [128,128]))\n",
    "        self.update_target_net()\n",
    "        \n",
    "    def est_q_value(self, state, action):\n",
    "        action = action.to(dtype=th.int64)\n",
    "        one_hot_action = th.nn.functional.one_hot(action, 2).to(dtype=th.float)\n",
    "        return .5 * self.max_val * (self.estimated_q_net(th.cat([state, one_hot_action], 1)).flatten() + 1.)\n",
    "    \n",
    "    def target_q_value(self, state, action):\n",
    "        action = action.to(dtype=th.int64)\n",
    "        one_hot_action = th.nn.functional.one_hot(action, 2).to(dtype=th.float)\n",
    "        return .5 * self.max_val * (self.target_q_net(th.cat([state, one_hot_action], 1)).flatten() + 1.)\n",
    "    \n",
    "    def estimate_value(self, state):\n",
    "        '''\n",
    "        The value is $V(s)=\\max_{a} Q(s,a), here we use est_q_value function\n",
    "        '''\n",
    "        raise NotImplemented\n",
    "    \n",
    "    def target_value(self, state):\n",
    "        '''\n",
    "        The value is $V(s)=\\max_{a} Q(s,a), here we use target_q_value function\n",
    "        '''\n",
    "        raise NotImplemented\n",
    "        \n",
    "    \n",
    "    def update_target_net(self):\n",
    "        '''\n",
    "        Copy q-network paramenters to the target network\n",
    "        '''\n",
    "        polyak_update(self.estimated_q_net.parameters(), self.target_q_net.parameters(), 1.)\n",
    "        # crude way to get rid of momentum and other such terms\n",
    "        self.opt = th.optim.RMSprop(self.estimated_q_net.parameters(), lr=0.0003)\n",
    "    \n",
    "    def train_one_step(self):\n",
    "        '''\n",
    "        take one step of training the q-function estimate\n",
    "        '''\n",
    "        raise NotImplemented\n",
    "\n",
    "        self.opt.zero_grad()\n",
    "        \n",
    "        # sample some states\n",
    "        state = self.gen_env.sample_state(self.batch_size)\n",
    "        \n",
    "        # sample a random action, that way all actions are trained\n",
    "        action = self.gen_env.sample_action(self.batch_size)\n",
    "        \n",
    "        # use the generative environment to observe reward and next state\n",
    "        \n",
    "        # use target q-function estimate with policy to get estimated future discounted reward\n",
    "        \n",
    "        # use bellman update described above to come up with a loss\n",
    "        # hint: use th.nn.functional.mse_loss for the loss\n",
    "        \n",
    "        # update q-function estimate\n",
    "        loss.backward()\n",
    "        self.opt.step()\n",
    "        return loss.item()\n",
    "    \n",
    "    def train(self, num_steps: int, target_update_interval: int, print_interval=1000):\n",
    "        '''\n",
    "        Train the q-function estimation to completion\n",
    "        '''\n",
    "        for i in range(num_steps):\n",
    "            if i % target_update_interval == 0:\n",
    "                self.update_target_net()\n",
    "            loss = self.train_one_step()\n",
    "            if i % print_interval == 0:\n",
    "                print(\"i, loss = \", i, loss)\n",
    "        \n",
    "    def _act_th(self, state):\n",
    "        '''\n",
    "        Given a state, return the action which gives highest estimated reward\n",
    "        '''\n",
    "        return NotImplemented\n",
    "        \n",
    "    def _act_np(self, state):\n",
    "        state_th = th.reshape(th.tensor(state, dtype=th.float32), (-1, ) + self.gen_env.observation_space.shape)\n",
    "        return self._act_th(state_th).detach().flatten().item()\n",
    "    \n",
    "    def act(self, state):\n",
    "        if isinstance(state, np.ndarray):\n",
    "            return self._act_np(state)\n",
    "        elif isinstance(state, th.Tensor):\n",
    "            return self._act_th(state)\n",
    "        else:\n",
    "            raise ValueError('unknown input type')\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train the q-function estimate, plot discouned value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qfunction = QFunction(cartpoleenv, .99)\n",
    "# call qfunction.train\n",
    "plot_value(cartpoleenv, calculate_value, qfunction.act, .99)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot undiscounted value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_value(cartpoleenv, calculate_value, qfunction.act, 1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run an episode for fun"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html, rew_sum = vm.display_episode(cartpoleenv, qfunction.act)\n",
    "print('rew_sum = ', rew_sum)\n",
    "HTML(html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_q_value(q_est):\n",
    "    N = 201\n",
    "    x_dot     = np.linspace(-1., 1., N)\n",
    "    theta_dot = np.linspace(-1., 1., N)\n",
    "\n",
    "    x_dot_g, theta_dot_g = np.meshgrid(x_dot, theta_dot)\n",
    "    x_dot_g = x_dot_g.flatten()\n",
    "    theta_dot_g = theta_dot_g.flatten()\n",
    "    state_np = np.stack([np.zeros_like(x_dot_g), x_dot_g, np.zeros_like(x_dot_g), theta_dot_g], axis=1)\n",
    "    state_th = th.tensor(state_np, dtype=th.float32)\n",
    "    est_value = q_est.estimate_value(state_th)\n",
    "    #est_value = q_est.act(state_th).to(dtype=th.float32)\n",
    "    print('max val = ', th.max(est_value).item())\n",
    "    print('mean val = ', th.mean(est_value).item())\n",
    "    print('mean val = ', th.min(est_value).item())\n",
    "    extent = (-1., 1., -1., 1.)\n",
    "    #plt.imshow(np.reshape(est_value.detach().numpy(), (N, N)), cmap=cm.jet, vmin=0., vmax=200., origin='lower', extent=extent)\n",
    "    plt.imshow(np.reshape(est_value.detach().numpy(), (N, N)), cmap=cm.jet, origin='lower', extent=extent)\n",
    "    plt.xlabel(\"starting velocity\")\n",
    "    plt.ylabel(\"starting angular velocity\")\n",
    "    plt.show()\n",
    "    plt.clf()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_q_value(qfunction)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
