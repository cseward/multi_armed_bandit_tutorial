import cv2
import numpy as np
import os
import re

class VideoManager(object):

    def __init__(self, filename='episode'):
        self.filename = filename
        self.rng = np.random.default_rng()
        self.reg_exp = re.compile(filename + '_\d*.webm')
        self.clean_up()

    def __del__(self):
        self.clean_up()

    def clean_up(self):
        for path in os.listdir('video'):
            if self.reg_exp.fullmatch(path):
                os.remove('video/' + path)

    def display_episode(self, env, policy):
        tag = self.rng.integers(2 ** 32)
        filepath = 'video/' + self.filename + '_' + str(tag) + '.webm'
        frame_list = []
        state = env.reset()
        frame = env.render('rgb_array')
        video_size = tuple(frame.shape[0:2][::-1])
        video = cv2.VideoWriter(filepath, cv2.VideoWriter_fourcc(*'VP90'), 30, video_size)
        video.write(frame)
        done = False
        rew_sum = 0.
        while not done:
            state, rew, done, _ = env.step(policy(state))
            frame = env.render('rgb_array')
            rew_sum += rew
            video.write(frame)
        env.close()
        video.release()

        return '<video width="600" height="400" controls><source src="' + filepath + '" type="video/webm"></video>', rew_sum
