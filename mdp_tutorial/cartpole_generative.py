"""
Classic cart-pole system implemented by Rich Sutton et al.
Copied from http://incompleteideas.net/sutton/book/code/pole.c
permalink: https://perma.cc/C9ZM-652R
"""
from typing import List, Tuple

import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
import torch as th

from gym.envs.classic_control.cartpole import CartPoleEnv

from generative_env import GenerativeEnv

class CartPoleGenerativeEnv(CartPoleEnv, GenerativeEnv):
    """
    Description:
        A pole is attached by an un-actuated joint to a cart, which moves along
        a frictionless track. The pendulum starts upright, and the goal is to
        prevent it from falling over by increasing and reducing the cart's
        velocity.

    Source:
        This environment corresponds to the version of the cart-pole problem
        described by Barto, Sutton, and Anderson

    Observation:
        Type: Box(4)
        Num     Observation               Min                     Max
        0       Cart Position             -4.8                    4.8
        1       Cart Velocity             -Inf                    Inf
        2       Pole Angle                -0.418 rad (-24 deg)    0.418 rad (24 deg)
        3       Pole Angular Velocity     -Inf                    Inf

    Actions:
        Type: Discrete(2)
        Num   Action
        0     Push cart to the left
        1     Push cart to the right

        Note: The amount the velocity that is reduced or increased is not
        fixed; it depends on the angle the pole is pointing. This is because
        the center of gravity of the pole increases the amount of energy needed
        to move the cart underneath it

    Reward:
        Reward is 1 for every step taken, including the termination step

    Starting State:
        All observations are assigned a uniform random value in [-0.05..0.05]

    Episode Termination:
        Pole Angle is more than 12 degrees.
        Cart Position is more than 2.4 (center of the cart reaches the edge of
        the display).
        Episode length is greater than 200.
        Solved Requirements:
        Considered solved when the average return is greater than or equal to
        195.0 over 100 consecutive trials.
    """

    def __init__(self):
        super(CartPoleGenerativeEnv, self).__init__()
        
    def sample_action(self, batch_size) -> th.Tensor:
        a = (self.np_random.uniform(low=0., high=1., size=(batch_size,)) > 0.5)
        action = th.tensor(a, dtype=th.int64)
        return action

    def sample_state(self, batch_size: int) -> th.Tensor:
        x     = self.np_random.uniform(low=-2. * self.x_threshold, high=2. * self.x_threshold, size=(batch_size,))
        x_dot = self.np_random.uniform(low=-self.x_threshold,      high=self.x_threshold, size=(batch_size,))
        theta = self.np_random.uniform(low=-2. * self.theta_threshold_radians, high=2. * self.theta_threshold_radians, size=(batch_size,))
        theta_dot = self.np_random.uniform(low=-self.theta_threshold_radians, high=self.theta_threshold_radians, size=(batch_size,))
        state = th.tensor(np.stack([x, x_dot, theta, theta_dot], axis=1), dtype=th.float32)
        return state

    def generative_step(self, state: th.Tensor, action: th.Tensor) -> Tuple[th.Tensor, th.Tensor, th.Tensor, List[dict]]:

        x = state[:,0]
        x_dot = state[:,1]
        theta = state[:,2]
        theta_dot = state[:,3]
        act = action.clone().flatten().to(dtype=th.float32)

        force = self.force_mag * (2. * act - 1.)

        costheta = th.cos(theta)
        sintheta = th.sin(theta)

        # For the interested reader:
        # https://coneural.org/florian/papers/05_cart_pole.pdf
        temp = (
            force + self.polemass_length * th.square(theta_dot) * sintheta
        ) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta * temp) / (
            self.length * (4.0 / 3.0 - self.masspole * costheta ** 2 / self.total_mass)
        )
        xacc = temp - self.polemass_length * thetaacc * costheta / self.total_mass

        if self.kinematics_integrator == "euler":
            x = x + self.tau * x_dot
            x_dot = x_dot + self.tau * xacc
            theta = theta + self.tau * theta_dot
            theta_dot = theta_dot + self.tau * thetaacc
        else:  # semi-implicit euler
            x_dot = x_dot + self.tau * xacc
            x = x + self.tau * x_dot
            theta_dot = theta_dot + self.tau * thetaacc
            theta = theta + self.tau * theta_dot

        state = th.stack([x, x_dot, theta, theta_dot], dim=1)

        done = ((th.abs(x) > self.x_threshold) + (th.abs(theta) > self.theta_threshold_radians))
        reward = th.ones_like(x)
        return state, reward, done, state.size()[0] * [{}]

alpha = 2.5
def heuristic_policy_np(state: np.ndarray) -> int:
    x, x_dot, theta, theta_dot = state
    if theta > 0:
        # here we'll generally go to the right. Only exception is if
        # the theta dot is quite small, so moving quickly left
        if -theta_dot > alpha * (1 - np.cos(theta)):
            return 0
        else:
            return 1
    else:
        if theta_dot > alpha * (1 - np.cos(theta)):
            return 1
        else:
            return 0

def heuristic_policy_th(state: th.Tensor) -> th.Tensor:
    '''
    same as heuristic_policy_np, but vectorized
    '''
    theta     = state[:,2]
    theta_dot = state[:,3]

    theta_positive = ((theta >  0.) * (-theta_dot <= alpha * (1 - np.cos(theta))))
    theta_negative = ((theta <= 0.) * ( theta_dot >  alpha * (1 - np.cos(theta))))
    return (theta_positive + theta_negative).to(dtype=th.int32)
