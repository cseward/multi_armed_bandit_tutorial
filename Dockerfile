from ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends python3.8 python3-pip python-opengl ffmpeg libsm6 libxext6 xvfb

RUN pip3 install numpy matplotlib sklearn gym opencv-python pyglet
RUN pip3 install torch==1.10.0+cpu -f https://download.pytorch.org/whl/cpu/torch_stable.html
RUN pip3 install jupyter
EXPOSE 8889
ENV DISPLAY :99
ADD run.sh /run.sh
RUN chmod a+x /run.sh
CMD /run.sh

