'''
two classes implementing two types of bandits. One with {0,1} answers, the other with gaussian noise answers
'''
from abc import abstractmethod
import numpy as np

class Bandit(object):

  def __init__(self, num_arms: int):
    self.num_arms = num_arms
    self.reset_called = False
    self.rng = np.random.default_rng()
    self.arm_values = np.zeros(num_arms)
    self.max_value  = np.max(self.arm_values)


  def seed(self, seed) -> None:
    '''seed the internal rng'''
    self.rng = np.random.default_rng(seed)

  def reset(self) -> None:
    '''sample values for each arm. If rng is not none, the arms are sampled from this'''
    self._reset()
    self.max_value  = np.max(self.arm_values)
    self.reset_called = True

  def pull(self, action: int) -> float:
    self._check_action_and_reset(action)
    return self._pull(action)

  def regret(self, action: int) -> float:
    '''get the regret of a specific action'''
    self._check_action_and_reset(action)
    return self.max_value - self.arm_values[action] 

  def _check_action_and_reset(self, action:int) -> None:
    '''error checking'''
    if action < 0 or action >= self.num_arms:
      raise ValueError("action must be be in the range [0, num_arms)")

    if not self.reset_called:
      raise RuntimeError("reset must be called before an arm can be pulled")

  @abstractmethod
  def _pull(self, action: int) -> float:
    '''return value of pulling a lever'''

  @abstractmethod
  def _reset(self) -> None:
    '''reset the bandit'''

class ContinuousBandit(Bandit):

  def __init__(self, num_arms: int):
    super(ContinuousBandit, self).__init__(num_arms)

  def _reset(self) -> None:
    self.arm_values = self.rng.random(self.num_arms)

  def _pull(self, action: int) -> float:
    return self.arm_values[action] + self.rng.standard_normal()
    

class BinaryBandit(Bandit):

  def __init__(self, num_arms: int):
    super(BinaryBandit, self).__init__(num_arms)

  def _reset(self) -> None:
    self.arm_values = self.rng.random(self.num_arms)

  def _pull(self, action: int) -> float:
    return float(self.rng.random() < self.arm_values[action])

