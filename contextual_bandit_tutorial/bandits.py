from typing import List, Tuple, Dict
from abc import abstractmethod
import numpy as np

sigmoid = lambda x : 1. / (1. + np.exp(-x))

class Context(object):
  '''
  Abstract base clase for contexts
  '''
  
  def __init__(self):
    pass

class ContextualBandit(object):

  def __init__(self, num_arms: int, num_contexts: int):
    self.num_arms = num_arms
    self.num_contexts = num_contexts
    self.reset_called = False
    self.rng = np.random.default_rng()

  def seed(self, seed) -> None:
    '''seed the internal rng'''
    self.rng = np.random.default_rng(seed)

  def reset(self) -> Context:
    '''Reset the bandit, return the bandit's current context'''
    self._reset()
    self.reset_called = True
    self.context = self._sample_context()
    return self.context

  def pull(self, action: int) -> Tuple[Context, float]:
    '''
    Take an action, recieve a reward and a Context
    '''
    self._check_action_and_reset(action)
    reward = self._pull(self.context, action)
    self.context = self._sample_context()
    return self.context, reward

  def regret(self, action: int) -> float:
    '''get the regret of a specific action'''
    self._check_action_and_reset(action)
    return self._max_value(self.context) - self._expected_value(self.context, action)

  def _check_action_and_reset(self, action: int) -> None:
    '''error checking'''
    if action < 0 or action >= self.num_arms:
      raise ValueError("action must be be in the range [0, num_arms)")

    if not self.reset_called:
      raise RuntimeError("reset must be called before an arm can be pulled")

  def _check_context_and_reset(self, context: int) -> None:
    '''error checking'''
    if context < 0 or context >= self.num_contexts:
      raise ValueError("action must be be in the range [0, num_arms)")

    if not self.reset_called:
      raise RuntimeError("reset must be called before an arm can be pulled")

  @abstractmethod
  def _pull(self, context: Context, action_list: int) -> float:
    '''return sampled value of pulling a lever'''

  @abstractmethod
  def _reset(self) -> None:
    '''reset the bandit'''

  @abstractmethod
  def _max_value(self, context: Context) -> float:
    '''return the maximum expected value possible for a given context'''

  @abstractmethod
  def _expected_value(self, context: Context, action: int) -> float:
    '''return the expected value for a given action'''

  @abstractmethod
  def _sample_context(self) -> Context:
    '''return a context'''

class CrossSellBandit(ContextualBandit):
  '''
  This bandit is a simple model of the cross selling problem. Given a context item,
  we recommend a cross sell item and recieve a real valued profit (or loss) from
  this cross-sell recommendation
  '''

  def __init__(self, num_arms: int, num_contexts, dimensionality: int):
    super(CrossSellBandit, self).__init__(num_arms, num_contexts)
    self.dim = dimensionality

  def get_action_embedding(self, action: int) -> np.ndarray:
    self._check_action_and_reset(action)
    return self.articles[action].copy()
 
  def get_context_embedding(self, context: int) -> np.ndarray:
    self._check_context_and_reset(context)
    return self.contexts[context].copy()

  def get_all_action_embeddings(self) -> np.ndarray:
    return self.articles.copy()

  def get_all_context_embeddings(self) -> np.ndarray:
    return self.contexts.copy()

  def _context_embedding(self, context: np.ndarray):
    context = np.reshape(context, [-1, self.dim])
    hidden_1 = np.maximum(np.matmul(context, self.context_fc_1) + self.context_bias_1, 0.)
    return np.matmul(hidden_1, self.context_fc_2)

  def _action_embedding(self, action: np.ndarray):
    action = np.reshape(action, [-1, self.dim])
    hidden_1 = np.maximum(np.matmul(action, self.action_fc_1) + self.action_bias_1, 0.)
    return np.matmul(hidden_1, self.action_fc_2)

  def _reset(self) -> None:
    # sample a bunch of articles
    self.articles = 2 * self.rng.integers(0, 2, (self.num_arms, self.dim), dtype=np.int32) - 1
    self.contexts = 2 * self.rng.integers(0, 2, (self.num_contexts, self.dim), dtype=np.int32) - 1

    # sample transition matrix
    num_hidden = 32
    self.context_fc_1 = (2. * self.rng.random((self.dim, num_hidden)) - 1.) / np.sqrt(self.dim)
    self.context_bias_1 = (2. * self.rng.random((1, num_hidden)) - 1.)
    self.context_fc_2 = (2. * self.rng.random((num_hidden, num_hidden)) - 1.) / np.sqrt(num_hidden)
    self.action_fc_1 = (2. * self.rng.random((self.dim, num_hidden)) - 1.) / np.sqrt(self.dim)
    self.action_bias_1 = (2. * self.rng.random((1, num_hidden)) - 1.)
    self.action_fc_2 = (2. * self.rng.random((num_hidden, num_hidden)) - 1.) / np.sqrt(num_hidden)

    context_embedding = self._context_embedding(self.contexts) # [num_arms, num_hidden]
    action_embedding  = self._action_embedding(self.articles)
    self._cross_sell_scores = np.matmul(context_embedding, action_embedding.T)
    self._cross_sell_scores /= np.std(self._cross_sell_scores)
    self._best_score = np.max(self._cross_sell_scores, axis=1)

  def _sample_context(self) -> int:
    '''return customer_idx, article_idx'''
    return self.rng.integers(0, self.num_contexts)

  def _pull(self, context: int, action: int) -> float:
    return self._expected_value(context, action) + self.rng.standard_normal()

  def _expected_value(self, context: int, action: int) -> float:
    return self._cross_sell_scores[context, action]

  def _max_value(self, context: int) -> float: 
    return self._best_score[context]









