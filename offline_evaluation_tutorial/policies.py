from typing import List, Tuple
from abc import abstractmethod, ABC
import numpy as np
import torch as th

from bandits import ContextualBandit, CrossSellBandit

class BasePolicy(ABC):
  def __init__(self, env):
    self.env = env
    self._rng = np.random.default_rng()

  @abstractmethod
  def act(self, context: int) -> int:
    raise NotImplementedError

class UniformCollectionPolicy(BasePolicy):
  def __init__(self, env: ContextualBandit):
    super(UniformCollectionPolicy, self).__init__(env)

  def act(self, context: int) -> int:
    return self._rng.choice(self.env.num_arms)

class RankArmPolicy(BasePolicy):
  def __init__(self, env: ContextualBandit, rank_arm_prob: np.ndarray):
    super(RankArmPolicy, self).__init__(env)
    self.rank_arm_prob = rank_arm_prob / np.sum(rank_arm_prob)
    self._setup()

  def _setup(self) -> None:
    self.expected_value_matrix = np.zeros((self.env.num_contexts, self.env.num_arms))
    self.argsort_value =         np.zeros((self.env.num_contexts, self.env.num_arms), dtype=np.int) 
    for i in range(self.env.num_contexts):
      for j in range(self.env.num_arms):
        self.expected_value_matrix[i,j] = self.env._expected_value(i, j)
      self.argsort_value[i,:] = np.argsort(-1. * self.expected_value_matrix[i])

  def act(self, context: int) -> int:
    idx = self._rng.choice(self.env.num_arms, p=self.rank_arm_prob)
    return self.argsort_value[context, idx]

class DataCollectionPolicy(RankArmPolicy):
  def __init__(self, env: ContextualBandit):
    x = np.arange(env.num_arms) / (env.num_arms - 1)
    y = .5 - np.abs(x - .5)
    y = np.maximum(y, 0.)
    super(DataCollectionPolicy, self).__init__(env, y)

class PolicyA(RankArmPolicy):
  def __init__(self, env: ContextualBandit):
    x = np.arange(env.num_arms) / (env.num_arms - 1)
    y = 50. * np.exp(-50. * x) + 1.
    super(PolicyA, self).__init__(env, y)

class PolicyB(RankArmPolicy):
  def __init__(self, env: ContextualBandit):
    x = np.arange(env.num_arms) / (env.num_arms - 1)
    y = .5 - 2.5 * np.abs(x - .2)
    y = np.maximum(y, 0.)
    super(PolicyB, self).__init__(env, y)
        
class PredictivePolicy(BasePolicy):
  def __init__(self, env: ContextualBandit, arm_pred_std_dev: np.ndarray):
    super(PredictivePolicy, self).__init__(env)
    self.arm_pred_std_dev = arm_pred_std_dev
    self._setup()

  def _setup(self) -> None:
    self.expected_value_matrix = np.zeros((self.env.num_contexts, self.env.num_arms))
    self.argsort_value =         np.zeros((self.env.num_contexts, self.env.num_arms), dtype=np.int) 
    # get the expected value for each context / action pair
    for i in range(self.env.num_contexts):
      for j in range(self.env.num_arms):
        self.expected_value_matrix[i,j] = self.env._expected_value(i, j)
      # argsort the whole thing, so first value is index of best arm
      self.argsort_value[i,:] = np.argsort(-1. * self.expected_value_matrix[i,:]) 
      #print('max value = ', np.min(self.expected_value_matrix[i,:]))
      #print('value at armgax = ', self.expected_value_matrix[i,self.argsort_value[i,199]])
      # add the noise. Here, the zero'th should be noise on best arm. 
      noise = self.arm_pred_std_dev * self._rng.normal(0., 1., self.env.num_arms)
      #print('adding noise = ', noise[199])
      self.expected_value_matrix[i, self.argsort_value[i,:]] += noise
      #print('new value at armgax = ', self.expected_value_matrix[i,self.argsort_value[i,199]])
      #assert False
      
      # now sort again
      self.argsort_value[i,:] = np.argsort(-1. * self.expected_value_matrix[i,:])

  def act(self, context: int) -> int:
    #print('max expected = ', np.max(self.expected_value_matrix[context,:]))
    a = self.argsort_value[context,0]
    #print('exp = ', self.expected_value_matrix[context, self.argsort_value[context,0]])
    #assert False
    return self.argsort_value[context,0]

  def predict(self, context: int, action: int) -> float: 
    return self.expected_value_matrix[context, action]

# lower loss, lower performance. We get this by putting noise on bad actions, causing one to seem good
class PredictivePolicyA(PredictivePolicy):
  def __init__(self, env: ContextualBandit):
    # higher variance for bad actions
    x = np.arange(env.num_arms) / (env.num_arms - 1)
    x = np.square(x)
    x *= 2.
    super(PredictivePolicyA, self).__init__(env, x)

class PredictivePolicyB(PredictivePolicy):
  def __init__(self, env: ContextualBandit):
    # higher variance for good actions
    x = 1. - np.arange(env.num_arms) / (env.num_arms - 1)
    x = np.square(x)
    x *= 4.
    super(PredictivePolicyB, self).__init__(env, x)
















