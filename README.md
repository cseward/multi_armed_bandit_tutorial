# multi_armed_bandit_tutorial

iPython notebook tutorial for learning bandit policies

# Using docker -- recommended

download the docker image saved as "jupyter-tutorial.tar.gz" at

https://drive.google.com/file/d/19TT5OCurkgT9Q0G5TvVbxomNl4joC4Fx/view?usp=sharing

this can be done in the command line by running

wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=19TT5OCurkgT9Q0G5TvVbxomNl4joC4Fx' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=19TT5OCurkgT9Q0G5TvVbxomNl4joC4Fx" -O jupyter-tutorial.tar.gz && rm -rf /tmp/cookies.txt

Load the docker image and run it with

sudo docker load -i jupyter-tutorial.tar.gz

sudo docker run -p 8889:8889 --rm -v $(pwd):/workspace -w /workspace jupyter-tutorial

You will see some stuff printed, copy the line that says

http://127.0.0.1:8889/?token=xxxx

into your browser of choice, and enjoy the tutorial

If you want to stop the image, then in a speperate terminal find the image and kill it by doing

sudo docker ps
sudo docker stop CONTAINER_ID

You can also stop the image by first running

sudo docker run -p 8889:8889 --rm -it -v $(pwd):/workspace -w /workspace jupyter-tutorial
 
(note the extra -i flag) and doing Ctrl-c twice. I don't recommend this, as just one Ctrl-c
(which happens commonly when trying to copy the notebook link) will
kill the xserver process but not the jupyter notebook, and lead to 
unexpected and hard to debug errors later.

# saving the docker image

sudo docker save jupyter-tutorial:latest | gzip > jupyter-tutorial.tar.gz

# Setup -- not recommended

if jupyter notebook not already installed, install it by

sudo apt-get install jupyter-notebook

verify that the notebook install worked by running `jupyter notebook`

rename your notebook (so that if you do git pull in the future your changes are not affected)

If the notebook doesn't run, set up a virtual environment.

Create a virtual environment, activate it and install numpy:

python3 -m venv bandit-venv

activate by `source bandit-venv-2/bin/activate`

You may need to do 

sudo apt-get instal python3.8-venv

first. Activate by

source bandit-venv/bin/activate
pip3 install numpy matplotlib sklearn gym opencv-python pyglet
pip3 install torch==1.8.2+cpu torchvision==0.9.2+cpu torchaudio==0.8.2 -f https://download.pytorch.org/whl/lts/1.8/torch_lts.html
pip3 install stable-baselines3

Now in this environment do `pip3 install jupyter`, and you should be good to go :)

NOTE: I have to do python3 -m venv bandit-venv to make this work


pip install ipykernel
python3 -m ipykernel install --user --name=bandit-venv

launch the notebook with 

jupyter notebook

then got to kernel -> change kernel -> bandit-venv to switch the kernel
